//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define MD_3DASHES			"---"
//------------------------------------------------------------------------------
// LOCAM VARIABLES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void mdTop( FILE *fd ) {
	fprintf(fd, "\n");
}
static void mdValue( FILE *fd, size_t iline) {
	char buffer[MAX_LINE_LENGTH];
	strcpy(buffer, "| ");
	for (size_t j = 0; j < table_number_of_columns; j++) {
		strcat(buffer, table_cell[iline][j].value);
		strcat(buffer, " |");
		if (j != table_number_of_columns - 1) strcat(buffer, " ");
	}
	fprintf(fd, "%s\n", buffer);
}
static void mdLine( FILE *fd ) {
	char buffer[MAX_LINE_LENGTH];
	strcpy(buffer, "| ");
	for (size_t j = 0; j < table_number_of_columns; j++) {
		switch (table_col[j].justification) {
		case JCENTER:
			strcat(buffer, ":-----:");
			break;
		case JRIGHT:
			strcat(buffer, "------:");
			break;
		case JLEFT:
			strcat(buffer, ":------");
			break;
		}
		strcat(buffer, " |");
		if (j != table_number_of_columns - 1) strcat(buffer, " ");
	}
	fprintf(fd, "%s\n", buffer);
}
static void mdBottom( FILE *fd ) {
	fprintf(fd, "\n");
}
//------------------------------------------------------------------------------
static char *getNextValue( char *pts, char *res) {
	if (*pts == '|') ++pts;
	while (*pts == ' ' && *pts != '\0') ++pts;
	if (*pts == '\0' || *pts == '|') {
		*res = '\0';
		return pts;
	}
	char *pt = res;
	while (*pts != '|' && *pts != '\0') {
		*pt = *pts;
		++pt;
		*pt = '\0';
		++pts;
	}
	pt = res + strlen(res) - 1;
	while (pt != res) {
		if (*pt != ' ') break;
		*pt = '\0';
		--pt;
	}
	return pts;
}
static boolean is_a_headercell_separator( char *str ) {
	for (size_t i = 0; i < strlen(str); i++) {
		if (str[i] == ':') {
			if (i != 0 && i != strlen(str) - 1)  return FALSE;
		} else {
			if (str[i] != '-') return FALSE;
		}
	}
	return TRUE;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _anais_writeMDfile( void ) {
	FILE *fd;
	if (NULL == (fd = fopen(mdfile, "w"))) {
		char buf[PATH_MAX];
		sprintf(buf, "Cannot open file '%s'!!!", mdfile);
		_anais_error(buf);
		return;
	}
	mdTop(fd);
	mdValue(fd, 0);
	mdLine(fd);
	for (size_t i = 1; i < table_number_of_lines; i++) {
		mdValue(fd, i);
	}
	mdBottom(fd);
	fclose(fd);
}
//------------------------------------------------------------------------------
void _anais_readMDfile( void ) {
	// There must be at least 3 dashes separating each header cell.
	// The outer pipes (|) are optional, and you don't need to make the
	// raw Markdown line up prettily
	FILE *fd;
	char str[MAX_READ_LENGTH];

	if (NULL == (fd = fopen(mdfile, "r")))
		_anais_fatal( "Cannot open file '%s'!!!", mdfile);
	// Compute number of columns and lines in the table.
	table_number_of_columns = table_number_of_lines = 0;
	boolean foundcoljustif = FALSE;
	while (fgets(str, MAX_READ_LENGTH, fd) != NULL) {
		if (str[strlen(str) - 1] == '\n') str[strlen(str) - 1] = '\0';
		if (strlen(str) == 0) continue;
		size_t nbc = 0;
		for (size_t i = 0; i < strlen(str); i++) {
			if (str[i] == '|' ) {
				if (i == 0) ++nbc;
				else if (str[i - 1] != '\\' && str[i + 1] != '\0') ++nbc;
			}
			if (! foundcoljustif) {
				if (NULL != strstr(str + i, MD_3DASHES)) {
					foundcoljustif = TRUE;
					--table_number_of_lines;
				}
			}
		}
		table_number_of_columns = MAX(table_number_of_columns, nbc);
		++table_number_of_lines;
	}
	// Tables allocation and initialization
	_anais_allocateTables(table_number_of_lines, table_number_of_columns);
	maximal_column_width = MAXWIDTH(table_number_of_columns);
	_anais_initializeTable(maximal_column_width);
	// Read table
	rewind(fd);
	size_t line_number = 0;
	size_t state = 0;
	boolean found_a_headercell = FALSE;
	char stemp[MAX_VALUE_LENGTH];
	while (fgets(str, MAX_READ_LENGTH, fd) != NULL) {
		if (str[strlen(str) - 1] == '\n') str[strlen(str) - 1] = '\0';
		if (strlen(str) == 0) continue;
		char *ptstr = str;
		for (size_t j = 0; j < table_number_of_columns; j++) {
			switch (state) {
			case 1:			// Colonne definition
				ptstr = getNextValue(ptstr, stemp);
				if (! is_a_headercell_separator(stemp)) {
					strcpy(table_cell[line_number][j].value, stemp);
					table_col[j].width = MAX(table_col[j].width, strlen(table_cell[line_number][j].value));
					table_col[j].width = MIN(table_col[j].width, maximal_column_width);
				} else {
					found_a_headercell = TRUE;
					if (stemp[0] == ':') {
						if (stemp[strlen(stemp) - 1] == ':')
							table_col[j].justification = JCENTER;
						else
							table_col[j].justification = JLEFT;
					} else {
						if (stemp[strlen(stemp) - 1] == ':')
							table_col[j].justification = JRIGHT;
						else
							table_col[j].justification = JLEFT;
					}
				}
				break;
			case 0:			// Column names
			default:		// Table lines
				ptstr = getNextValue(ptstr, table_cell[line_number][j].value);
				table_col[j].width = MAX(table_col[j].width, strlen(table_cell[line_number][j].value));
				table_col[j].width = MIN(table_col[j].width,maximal_column_width);
				break;
			}
		}
		++state;
		if (found_a_headercell) found_a_headercell = FALSE;
		else ++line_number;
	}
	fclose(fd);
}
//------------------------------------------------------------------------------
#ifdef ANAIS_FILE
void _anais_writeANAISfile ( void ) {
	FILE *fd;
	if (NULL == (fd = fopen(anaisfile, "wb"))) {
		char buf[PATH_MAX];
		sprintf(buf, "Cannot open file '%s'!!!", anaisfile);
		_anais_error(buf);
		_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
		_anais_moveCursor();
		return;
	}
	fwrite((void *) &table_number_of_lines, sizeof(int), 1, fd);
	fwrite((void *) &table_number_of_columns, sizeof(int), 1, fd);
	fwrite(table_col, sizeof(struct s_tablecol), table_number_of_columns, fd);
	for (size_t i = 0; i < table_number_of_lines; i++)
		fwrite(table_cell[i], sizeof(struct s_tablecell), table_number_of_columns, fd);
	fclose(fd);
}
//------------------------------------------------------------------------------
void _anais_readANAISfile( void ) {
	FILE *fd;
	if (NULL == (fd = fopen(anaisfile, "rb")))
		_anais_fatal( "Cannot open file '%s'!!!", anaisfile);
	if (1 != fread((void *) &table_number_of_lines, sizeof(int), 1, fd))
		_anais_fatal("Error while reaading file %s. Abort!",  anaisfile);
	if (1 != fread((void *) &table_number_of_columns, sizeof(int), 1, fd))
		_anais_fatal("Error while reaading file %s. Abort!",  anaisfile);
	_anais_allocateTables(table_number_of_lines, table_number_of_columns);
	if (table_number_of_columns != fread(table_col, sizeof(struct s_tablecol), table_number_of_columns, fd))
		_anais_fatal("Error while reaading file %s. Abort!",  anaisfile);
	for (size_t i = 0; i < table_number_of_lines; i++)
		if (table_number_of_columns != fread(table_cell[i], sizeof(struct s_tablecell), table_number_of_columns, fd))
			_anais_fatal("Error while reaading file %s. Abort!",  anaisfile);
	fclose(fd);

	for (size_t i = 0; i < table_number_of_columns; i++)
		table_col[i].width = MIN(table_col[i].width, maximal_column_width);;
}
#endif
//==============================================================================
