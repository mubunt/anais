//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS CODES
//------------------------------------------------------------------------------
#define SET_ATTRIBUT(att)						{ strcat(newvalue, att); dest = newvalue + strlen(newvalue); }
#define PUT_CHAR(c)								*dest = c; ++dest; *dest = 0;
#define PRINT_EMPTY(buf, width)					sprintf(b, "%*s", (int) (width), C_SPACE); \
												strcat(buf, b); \
												relative_col_of_end_of_value = 0;

#define PRINT_CENTER(buf, value, len, width)	n = width - len; \
												depending_on_justification = 0; \
												switch (n) { \
												case 0: sprintf(b, "%s", value); \
														break; \
												case 1: sprintf(b, "%s ", value); \
														break; \
												default: sprintf(b, "%*s%s%*s", (int) (n / 2), C_SPACE, value, (int)(n - (n / 2)), C_SPACE); \
														depending_on_justification = n / 2; \
														break; \
												} \
												strcat(buf, b); \
												relative_col_of_end_of_value = (n / 2) + len;

#define PRINT_RIGHT(buf, value, len, width)		n = width - len; \
												switch (n) { \
												case 0: sprintf(b, "%s", value); \
														depending_on_justification = 0; \
														break; \
												default: sprintf(b, "%*s%s", (int)(width - len), C_SPACE, value); \
														depending_on_justification = width - len; \
														break; \
												} \
												strcat(buf, b); \
												relative_col_of_end_of_value = width;

#define PRINT_LEFT(buf, value, len, width)		n = width - len; \
												depending_on_justification = 0; \
												switch (n) { \
												case 0:	sprintf(b, "%s", value); \
														break; \
												default: sprintf(b, "%-s%*s", value, (int)(width - len), C_SPACE); \
														break; \
												} \
												strcat(buf, b); \
												relative_col_of_end_of_value = len;

#define STRONG_VALUE(buffer, value)				strcpy(buffer, MD_STRONG1); \
												strcat(buffer, value); \
												strcat(buffer, MD_STRONG1);
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static boolean					strong_attribut;	// 'strong_attribut' attribut on going between 2 lines
static boolean					emphasis_attribut;	// 'emphasis_attribut' attribut on going between 2 lines
static boolean					strike_attribut;	// 'strike_attribut' attribut on going between 2 lines
static char 					ANAIS[]				= "ANAIS, THE MARKDOWN TABLES TEXTUAL GENERATOR";
static const char 				menu_msg[35]		= "[For menus, type: F2, F3 or F4] ";
static const char 				indicator[5]		= "  ";
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void title(char *title) {
	char buffer[MAX_LINE_LENGTH];
	size_t n = ((size_t) nbCOLS) - (strlen(menu_msg) + strlen(indicator) + 1);
	if (strlen(title) > n) {
		char c = title[n];
		title[n] = '\0';
		DISPLAY_AT(1, strlen(indicator) + 2, ESC_STRONG_ON "%s" ESC_ATTRIBUTSOFF, title);
		title[n] = c;
	} else
		DISPLAY_AT(1, strlen(indicator) + 2, ESC_STRONG_ON "%s" ESC_ATTRIBUTSOFF, title);
	DISPLAY_AT(1, (int) (n + strlen(indicator) + 1), ESC_EMPHASIS_ON "%s" ESC_ATTRIBUTSOFF, menu_msg);
	*buffer = '\0';
	for (int i = 0; i < nbCOLS; i++) strcat(buffer, C_H);
	DISPLAY_AT(2, 1, ESC_STRONG_ON "%s" ESC_ATTRIBUTSOFF, buffer);
	_anais_save_indicator();
}
//------------------------------------------------------------------------------
static char *give_part_of( boolean processDecoration, char *start, size_t requestedlength, size_t *actuallength ) {
	*actuallength = 0;
	boolean backslash = FALSE;
	size_t indexoflastspace = 0;
	char *addroflastspace = NULL;
	// Looking for space near the max limit.
	while (*start != '\0') {
		if (backslash) {
			backslash = FALSE;
			++*actuallength;
			++start;
			continue;
		}
		if (*start == '\\') {
			backslash = TRUE;
			++start;
			continue;
		}
		if (processDecoration) {
			if (strncmp(start, MD_STRONG1, 2) == 0
			        || strncmp(start, MD_STRONG2, 2) == 0
			        || strncmp(start, MD_STRIKE, 2) == 0) {
				start = start + 2;
				continue;
			}
			if (strncmp(start, MD_EMPHASIS1, 1) == 0
			        || strncmp(start, MD_EMPHASIS2, 1) == 0) {
				++start;
				continue;
			}
		}
		if (*start == ' ') {
			indexoflastspace = *actuallength;
			addroflastspace = start;
		}
		++*actuallength;
		++start;
		if (*actuallength == requestedlength) break;
	}
	if (*start == '\0') return start;				// Line  shorter than the demand
	if (indexoflastspace == 0) return start; 		// Right length but no space available
	*actuallength = indexoflastspace;
	return addroflastspace;
}
//------------------------------------------------------------------------------
static char *process_attributs(char *value, char *attributsoff) {
	char *newvalue = malloc((5 * strlen(value)) * sizeof(char));
	char *dest = newvalue;
	*dest = 0;
	boolean backslash = FALSE;
	while (*value != '\0') {
		if (backslash) {
			backslash = FALSE;
			PUT_CHAR(*value);
			++value;
			continue;
		}
		if (*value == '\\') {
			backslash = TRUE;
			++value;
			continue;
		}
		if (strncmp(value, MD_STRONG1, 2) == 0 || strncmp(value, MD_STRONG2, 2) == 0) {
			if (strong_attribut) {
				SET_ATTRIBUT(attributsoff)
				if (strike_attribut) SET_ATTRIBUT(ESC_STRIKE_ON);
				if (emphasis_attribut) SET_ATTRIBUT(ESC_EMPHASIS_ON);
			} else
				SET_ATTRIBUT(ESC_STRONG_ON);
			strong_attribut = ~strong_attribut;
			++value;
			++value;
			continue;
		}
		if (strncmp(value, MD_STRIKE, 2) == 0) {
			if (strike_attribut) {
				SET_ATTRIBUT(attributsoff)
				if (strong_attribut) SET_ATTRIBUT(ESC_STRONG_ON);
				if (emphasis_attribut) SET_ATTRIBUT(ESC_EMPHASIS_ON);
			} else
				SET_ATTRIBUT(ESC_STRIKE_ON);
			strike_attribut = ~ strike_attribut;
			++value;
			++value;
			continue;
		}
		if (strncmp(value, MD_EMPHASIS1, 1) == 0 || strncmp(value, MD_EMPHASIS2, 1) == 0) {
			if (emphasis_attribut) {
				SET_ATTRIBUT(attributsoff)
				if (strong_attribut) SET_ATTRIBUT(ESC_STRONG_ON);
				if (strike_attribut) SET_ATTRIBUT(ESC_STRIKE_ON);
			} else
				SET_ATTRIBUT(ESC_EMPHASIS_ON);
			emphasis_attribut = ~ emphasis_attribut;
			++value;
			continue;
		}
		PUT_CHAR(*value);
		++value;
	}
	// If some attributs have been set, then put off them.
	if (strike_attribut || emphasis_attribut) {
		SET_ATTRIBUT(attributsoff);
		SET_ATTRIBUT(ESC_STRONG_ON);
	}
	return newvalue;
}
//------------------------------------------------------------------------------
static void displayLine( int *screen_line, int screen_col, const char *C1, const char *C2, const char *C3, const char *C4 ) {
	char buffer[MAX_LINE_LENGTH];
	strcpy(buffer, C1);
	strcat(buffer, C2);
	for (size_t i = 0; i < table_number_of_columns; i++) {
		for (size_t j = 0; j < table_col[i].width; j++) strcat(buffer, C2);
		strcat(buffer, C2);
		if (i == table_number_of_columns - 1) {
			strcat(buffer, C4);
		} else {
			strcat(buffer, C3);
			strcat(buffer, C2);
		}
	}
	DISPLAY_AT((*screen_line)++, screen_col, "%s", buffer);
}
//------------------------------------------------------------------------------
static boolean displayValue( e_viewing_mode viewing_mode, int *screen_line, int screen_col, int table_line,
                             boolean lineToBeColored, int colToBeColored ) {
	char buffer[MAX_LINE_LENGTH], b[MAX_LINE_LENGTH], attributsoff[32];
	char *expansedvalue;
	boolean still_lines_to_print = FALSE;
	size_t depending_on_justification = 0, n;

	strcpy(buffer, C_V);
	strcat(buffer, C_SPACE);

	int relative_start_of_column = 2;
	size_t relative_col_of_end_of_value = 0;
	for (size_t j = 0; j < table_number_of_columns; j++) {
		strcpy(attributsoff, ESC_ATTRIBUTSOFF);
		if (lineToBeColored || colToBeColored == (int) j) {
			strcat(buffer, ESC_REVERSE_ON );
			strcat(attributsoff, ESC_REVERSE_ON);
		}
		table_cell[table_line][j].startCol = screen_col + relative_start_of_column;
		strong_attribut = emphasis_attribut = strike_attribut = FALSE;
		size_t length;
		if (viewing_mode == VIEW)
			length = _anais_count_number_of_effective_characters(table_cell[table_line][j].ptdisplay);
		else
			length = strlen(table_cell[table_line][j].ptdisplay);
		if (length == 0) {
			PRINT_EMPTY(buffer, table_col[j].width);
			if (table_cell[table_line][j].endLine == -1) {
				table_cell[table_line][j].endLine = *screen_line;
			}
		} else {
			if (length <= table_col[j].width) {
				if (table_line == 0) {
					STRONG_VALUE(b, table_cell[table_line][j].ptdisplay);
					expansedvalue = process_attributs(b, attributsoff);
					PRINT_CENTER(buffer, expansedvalue, length, table_col[j].width);
				} else {
					if (viewing_mode == VIEW)
						expansedvalue = process_attributs(table_cell[table_line][j].ptdisplay, attributsoff);
					else
						expansedvalue = table_cell[table_line][j].ptdisplay;
					switch (table_col[j].justification) {
					case JCENTER:
						PRINT_CENTER(buffer, expansedvalue, length, table_col[j].width);
						break;
					case JRIGHT:
						PRINT_RIGHT(buffer, expansedvalue, length, table_col[j].width);
						break;
					case JLEFT:
						PRINT_LEFT(buffer, expansedvalue, length, table_col[j].width);
						break;
					}
				}
				table_cell[table_line][j].ptdisplay += strlen(table_cell[table_line][j].ptdisplay);
			} else {
				char *ptend = give_part_of((viewing_mode == VIEW), table_cell[table_line][j].ptdisplay, table_col[j].width, &length);
				char c = *ptend;
				*ptend = '\0';
				if (table_line == 0) {
					STRONG_VALUE(b, table_cell[table_line][j].ptdisplay);
					expansedvalue = process_attributs(b, attributsoff);
					PRINT_CENTER(buffer, expansedvalue, length, table_col[j].width);
				} else {
					if (viewing_mode == VIEW)
						expansedvalue = process_attributs(table_cell[table_line][j].ptdisplay, attributsoff);
					else
						expansedvalue = table_cell[table_line][j].ptdisplay;
					switch (table_col[j].justification) {
					case JCENTER:
						PRINT_CENTER(buffer, expansedvalue, length, table_col[j].width);
						break;
					case JRIGHT:
						PRINT_RIGHT(buffer, expansedvalue, length, table_col[j].width);
						break;
					case JLEFT:
						PRINT_LEFT(buffer, expansedvalue, length, table_col[j].width);
						break;
					}
				}
				*ptend = c;
				if (*ptend == ' ') ++ptend;
				table_cell[table_line][j].ptdisplay = ptend;
				still_lines_to_print = TRUE;
			}
			table_cell[table_line][j].endLine = *screen_line;

			if (viewing_mode == VIEW) free(expansedvalue);

			struct s_tabledisplay *pt_tabledisplay = _anais_allocateDisplayLine();
			size_t n = (size_t) table_line * table_number_of_columns + j;
			if (table_display[n] == NULL) {
				table_display[n] = pt_tabledisplay;
			} else {
				struct s_tabledisplay *pt = table_display[n];
				while (pt->next != NULL) pt = pt->next;
				pt->next = pt_tabledisplay;
				pt_tabledisplay->previous = pt;

			}
			pt_tabledisplay->startLineValue = *screen_line;
			pt_tabledisplay->startColValue = screen_col + relative_start_of_column + (int) depending_on_justification;
			pt_tabledisplay->endColValue = pt_tabledisplay->startColValue + (int) length;

		}
		relative_start_of_column += (int) table_col[j].width;
		if (lineToBeColored || colToBeColored == (int) j) strcat(buffer, ESC_ATTRIBUTSOFF);
		strcat(buffer, C_SPACE);
		strcat(buffer, C_V);
		if (j != table_number_of_columns - 1)
			strcat(buffer, C_SPACE);
		relative_start_of_column += 3;
	}
	DISPLAY_AT((*screen_line)++, screen_col, "%s", buffer);
	return still_lines_to_print;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _anais_clearScreen() {
	CLEAR_ENTIRE_SCREEN();
	title(ANAIS);
}
//------------------------------------------------------------------------------
void _anais_save_indicator() {
	char buffer[16];
	if (modification_done_and_not_saved) {
		sprintf(buffer, ESC_COLOR "%s" ESC_ATTRIBUTSOFF, BACKGROUND(red), (char *) indicator);
	} else {
		sprintf(buffer, ESC_COLOR "%s" ESC_ATTRIBUTSOFF, BACKGROUND(green), (char *) indicator);
	}
	DISPLAY_AT(1, 1, "%s", buffer);
	FLUSH();
}
//------------------------------------------------------------------------------
void _anais_displayTable( e_viewing_mode viewing_mode, int lineToBeColored, int colToBeColored ) {
	char buffer[MAX_LINE_LENGTH];
	int iline = initial_line_number;
	int icol = initial_col_number;
	boolean still_lines_to_print;
	// Initialization for value processing
	for (size_t i = 0; i < table_number_of_lines; i++) {
		for (size_t j = 0; j < table_number_of_columns; j++) {
			table_cell[i][j].ptdisplay = table_cell[i][j].value;
			table_cell[i][j].endLine = -1;
			table_cell[i][j].startCol = -1;
		}
	}
	if (table_display != NULL) _anais_freeDisplayTable();
	_anais_allocateDisplayTable();
	// CLEAR and TITLE
	_anais_clearScreen();
	// TOP
	displayLine(&iline, icol, C_ULC, C_H, C_TT, C_URC);
	// HEADER
	do {
		still_lines_to_print = displayValue(viewing_mode, &iline, icol, 0, FALSE, colToBeColored);
	} while (still_lines_to_print);
	// HEADER SEPARATION
	displayLine(&iline, icol, D_VHR, D_H, D_X, D_VHL);
	// VALUES & SEPARATION
	for (int i = 1; i < (int) table_number_of_lines; i++) {
		do {
			still_lines_to_print = displayValue(viewing_mode, &iline, icol, i, (i == lineToBeColored), colToBeColored);
		} while (still_lines_to_print);
		if (i != (int) table_number_of_lines - 1)
			displayLine(&iline, icol, C_VHR, C_H, C_X, C_VHL);
	}
	// BOTTOM
	displayLine(&iline, icol, C_BLC, C_H, C_BT, C_BRC);
	FLUSH();
}
//==============================================================================
