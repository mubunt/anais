//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _anais_fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	_anais_endScreen();
	fprintf(stderr, "\n" ESC_COLOR ESC_STRONG_ON "%s%s" ESC_ATTRIBUTSOFF "\n\n", FOREGROUND(red), FATALPREFIX, buff);
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
void _anais_getTerminalSize( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		_anais_fatal("%s", "Cannot get terminal size Abort!");
	*lines = w.ws_row;
	*cols = w.ws_col;
}
//------------------------------------------------------------------------------
void _anais_setTerminalMode( e_terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &cooked)) {
			_anais_fatal("%s", "Cannot set the parameter 'MODE_ECHOED' associated with the terminal. Abort!");
		}
		break;
	case MODE_RAW:
		if (0 != tcgetattr(STDIN_FILENO, &cooked)) {
			_anais_fatal("%s", "Cannot set the parameter 'MODE_RAW' associated with the terminal. Abort!");
		}
		raw = cooked;
		cfmakeraw(&raw);
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &raw)) {
			_anais_fatal("%s", "Cannot set the parameter 'MODE_RAW/TCSANOW' associated with the terminal. Abort!");
		}
		break;
	}
}
//------------------------------------------------------------------------------
void _anais_getTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		_anais_fatal("%s", "The terminfo database could not be found. Abort!");
		break;
	case 0:
		_anais_fatal("%s", "There is no entry for this terminal in the terminfo database. Abort!");
		break;
	default:
		break;
	}
	// Get various string-valued capabilities.
	static char sbuf[TERMSBUF_SIZE];
	char *sp = sbuf;
	if (NULL == (screeninit = tgetstr("ti", &sp)))
		_anais_fatal("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database. Abort!");
	if (NULL == (screendeinit = tgetstr("te", &sp)))
		_anais_fatal("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database. Abort!");
	if (NULL == (clear = tgetstr("cl", &sp)))
		_anais_fatal("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database. Abort!");
	if (NULL == (move = tgetstr("cm", &sp)))
		_anais_fatal("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database. Abort!");
}
//------------------------------------------------------------------------------
void _anais_getCursorPosition( int *line, int *column) {
	char buf[64];
	char cmd[]=GET_CURSOR;
	struct termios save, raw;

	//sFLUSH();
	memset((void *)&save, 0, sizeof(save));
	memset((void *)&raw, 0, sizeof(raw));
	tcgetattr(0, &save);
	cfmakeraw(&raw);
	tcsetattr(0, TCSANOW, &raw);
	ssize_t notused = write(1, cmd, sizeof(cmd));
	notused = read (0, buf, sizeof(buf));
	tcsetattr(0, TCSANOW, &save);
	sscanf(buf + 2,"%d;%dR", line, column);
}
//------------------------------------------------------------------------------
size_t _anais_count_characters_out_escaped_codes( char *str ) {
	unsigned char *s = (void *)str;
	size_t non_escape_chars = 0;
	size_t non_printable_nbchars = 0;
	int state = 0;
	while (1) {
		if (*s == '\0') break;
		switch (state) {
		case 0:
			if (*s == '\033') state = 1;
			else if (*s >= 128) ++non_printable_nbchars;
			else ++non_escape_chars;
			break;
		case 1:
			if (*s == '[') state = 2;
			break;
		case 2:
			if (*s >= '0' && *s <= '9') state = 3;
			else if (*s == 'm') state = 0;
			break;
		case 3:
			if (*s >= '0' && *s <= '9') state = 4;
			else if (*s == ';') state = 2;
			break;
		case 4:
			if (*s == 'm') state = 0;
			else if (*s == ';') state = 2;
			break;
		}
		++s;
	}
	return non_escape_chars + non_printable_nbchars / 3;
}
//------------------------------------------------------------------------------
size_t _anais_compute_index_out_escaped_codes( char *str, size_t idx) {
	unsigned char *s = (void *)str;
	size_t non_escape_chars = 0;
	size_t escape_chars = 0;
	size_t non_printable_nbchars = 0;
	int state = 0;
	while (1) {
		if (idx == non_escape_chars + non_printable_nbchars / 3 && (non_printable_nbchars % 3) == 0)
			break;
		if (*s == '\0')
			break;
		switch (state) {
		case 0:
			if (*s == '\033') {
				++escape_chars;
				state = 1;
			} else if (*s >= 128) ++non_printable_nbchars;
			else ++non_escape_chars;
			break;
		case 1:
			if (*s == '[') {
				++escape_chars;
				state = 2;
			}
			break;
		case 2:
			if (*s >= '0' && *s <= '9') {
				++escape_chars;
				state = 3;
			} else if (*s == 'm') {
				++escape_chars;
				state = 0;
			}
			break;
		case 3:
			if (*s >= '0' && *s <= '9') {
				++escape_chars;
				state = 4;
			} else if (*s == ';') {
				++escape_chars;
				state = 2;
			}
			break;
		case 4:
			if (*s == 'm') {
				++escape_chars;
				state = 0;
			} else if (*s == ';') {
				++escape_chars;
				state = 2;
			}
			break;
		}
		++s;
	}
	return non_escape_chars + escape_chars + non_printable_nbchars;
}
//------------------------------------------------------------------------------
size_t _anais_count_number_of_effective_characters( char *str )  {
	size_t len = 0;
	boolean backslash = FALSE;
	while (*str != '\0') {
		if (backslash) {
			backslash = FALSE;
			++len;
			++str;
			continue;
		}
		if (*str == '\\') {
			backslash = TRUE;
			++str;
			continue;
		}
		if (strncmp(str, MD_STRONG1, 2) == 0
		        || strncmp(str, MD_STRONG2, 2) == 0
		        || strncmp(str, MD_STRIKE, 2) == 0) {
			str = str + 2;
			continue;
		}
		if (strncmp(str, MD_EMPHASIS1, 1) == 0
		        || strncmp(str, MD_EMPHASIS2, 1) == 0) {
			++str;
			continue;
		}
		++len;
		++str;
	}
	return len;
}
//==============================================================================
