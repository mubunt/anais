//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static const char *error_title		= "ANAIS ERROR";
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void readChoice( void ) {
	boolean selection = TRUE;
	HIDE_CURSOR();
	while (1) {
		int n = _anais_incomingKey();
		switch (n) {
		case _ok:
			n = ANAIS_OK;
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (n == ANAIS_OK) break;
	}
	SHOW_CURSOR();
}
//------------------------------------------------------------------------------
static void displayOK( int *iline, int icol, size_t width ) {
#define OK 	"  OK  "
	char okhighlighted[MAX_LINE_LENGTH];
	size_t p = 4 * (width - strlen(OK) - 2) / 5;
	sprintf(okhighlighted, ESC_STRONG_ON "%s", C_V);
	for (size_t i = 0; i < p; i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, ESC_REVERSE_ON);
	strcat(okhighlighted, OK);
	strcat(okhighlighted, ESC_ATTRIBUTSOFF);
	strcat(okhighlighted, ESC_STRONG_ON);
	for (size_t i = 0; i < width - p - strlen(OK) - 2; i++) strcat(okhighlighted, " ");
	strcat(okhighlighted, C_V);
	strcat(okhighlighted, ESC_ATTRIBUTSOFF);
	DISPLAY_AT((*iline)++, icol, "%s", okhighlighted);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _anais_error( char *label ) {
	// The width of the error is the smallest between 2/3 of terminal width and the width
	// of the label plus edges (2 * 2 characters)
	size_t width= MIN( (size_t) (2 * nbCOLS / 3), strlen(label) + 4);
	// With an adaptation if the title is longer !!!
	width = MAX(width, strlen(error_title));
	// Position of the error (7 = frame + title + empty lines + button)
	int line = (nbLINES -16) / 2;
	int col = (nbCOLS -  (int) width) / 2;
	// Display error
	_anais_displayTop(&line, col, width, (char *) error_title);
	_anais_displayEmptyLine(&line, col, width);
	_anais_displayLabel(&line, col, width - 4, label);
	_anais_displayEmptyLine(&line, col, width);
	displayOK(&line, col, width);
	_anais_displayEmptyLine(&line, col, width);
	_anais_displayBottom(line, col,  width);
	FLUSH();
	// Read selection
	readChoice();
}
//------------------------------------------------------------------------------
void _anais_displayTop( int *iline, int icol, size_t width, char *title ) {
	char buffer[MAX_LINE_LENGTH];
	sprintf(buffer, ESC_STRONG_ON "%s", C_ULC);
	for (size_t i = 0; i < width - 2; i++) strcat(buffer, C_H);
	strcat(buffer, C_URC);
	strcat(buffer, ESC_ATTRIBUTSOFF);
	DISPLAY_AT((*iline)++, icol, "%s", buffer);

	if (strlen(title) != 0) {
		size_t n = (width - strlen(title)) / 2 - 2;
		sprintf(buffer, ESC_STRONG_ON "%s%*s%s%*s%s" ESC_ATTRIBUTSOFF, C_V, (int) n, " ", title, (int) (width - strlen(title) - n - 2), " ", C_V);
		DISPLAY_AT((*iline)++, icol, "%s", buffer);
		sprintf(buffer, ESC_STRONG_ON "%s", D_VHR);
		for (size_t i = 0; i < width - 2; i++) strcat(buffer, D_H);
		strcat(buffer, D_VHL);
		strcat(buffer, ESC_ATTRIBUTSOFF);
		DISPLAY_AT((*iline)++, icol, "%s", buffer);
	}
}
//------------------------------------------------------------------------------
void _anais_displayBottom( int iline, int icol, size_t width ) {
	char buffer[MAX_LINE_LENGTH];
	sprintf(buffer, ESC_STRONG_ON "%s", C_BLC);
	for (size_t i = 0; i < width - 2; i++) strcat(buffer, C_H);
	strcat(buffer, C_BRC);
	strcat(buffer, ESC_ATTRIBUTSOFF);
	DISPLAY_AT(iline, icol, "%s", buffer);
}
//------------------------------------------------------------------------------
void _anais_displayEmptyLine( int *iline, int icol, size_t width ) {
	char buffer[MAX_LINE_LENGTH];
	sprintf(buffer, ESC_STRONG_ON "%s", C_V);
	for (size_t i = 0; i < width - 2; i++) strcat(buffer, " ");
	strcat(buffer, C_V);
	strcat(buffer, ESC_ATTRIBUTSOFF);
	DISPLAY_AT((*iline)++, icol, "%s", buffer);
}
//------------------------------------------------------------------------------
void _anais_displayLabel( int *iline, int icol, size_t width, char *label ) {
	char buffer[MAX_LINE_LENGTH];
	size_t m = MAX((size_t) width, strlen(label));
	char *buffline = malloc(sizeof(char) * (m + 1));
	char *ptb = label;
	size_t n;

	while (1) {
		if (_anais_count_characters_out_escaped_codes(ptb) > (size_t) width) {
			n =_anais_compute_index_out_escaped_codes(ptb, (size_t) width);
			size_t i;
			for (i = 0; i < n; i++)
				if (ptb[i] == '\n') break;
			if (ptb[i] == '\n') {
				n = i + 1;
			} else {
				i = n - 1;
				while (ptb[i] != '\n' && ptb[i] != ' ' && i != 0) --i;
				if (i != 0) n = i + 1;
			}
			strncpy(buffline, ptb, n);
			buffline[n - 1] = '\0';
		} else {
			size_t i = 0;
			while (ptb[i] != '\n' && ptb[i] != '\0') ++i;
			if (ptb[i] == '\n')
				n = i + 1;
			else
				n = strlen(ptb);
			strcpy(buffline, ptb);
		}
		m = _anais_count_characters_out_escaped_codes(buffline);
		if (m < (size_t) width)
			for (size_t i = m; i < (size_t) width; i++) strcat(buffline, " ");

		sprintf(buffer, ESC_STRONG_ON "%s %-*s %s" ESC_ATTRIBUTSOFF, C_V, (int) width, buffline, C_V);
		DISPLAY_AT((*iline)++, icol, "%s", buffer);
		ptb += n;
		if (ptb >= (label + strlen(label))) break;
	}
	free(buffline);
}
//==============================================================================
