//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------
#ifndef ANAIS_DATA_H
#define ANAIS_DATA_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <termcap.h>
#include <termios.h>
#include <stdarg.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define ANAIS_OK							0
#define ANAIS_ABORT							-1
#define ANAIS_BACK							-2

#define ANAIS_SUFFIX						".anais"
#define MD_SUFFIX							".md"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define FATALPREFIX							"(ANAIS) "
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define MD_STRONG1							"**"
#define MD_STRONG2							"__"
#define MD_EMPHASIS1						"*"
#define MD_EMPHASIS2						"_"
#define MD_STRIKE							"~~"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define ESC_ATTRIBUTSOFF					"\033[0m"
#define ESC_STRONG_ON						"\033[1m"
#define ESC_EMPHASIS_ON						"\033[3m"
#define ESC_REVERSE_ON						"\033[7m"
#define ESC_STRIKE_ON						"\033[9m"
#define ESC_COLOR							"\033[%dm"
#define GET_CURSOR							"\033[6n"

#define BLINKING_BLOCK						1
#define STEADY_BLOCK						2
#define BLINKING_UNDERLINE					3
#define STEADY_UNDERLINE					4
#define BLINKING_BAR						5
#define STEADY_BAR							6
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define C_SPACE								" "
#define	C_H									"─"
#define C_V									"│"
#define C_VHR 								"├"
#define C_VHL 								"┤"
#define C_TT								"┬"
#define C_X									"┼"
#define C_BT								"┴"
#define	C_ULC								"╭"
#define	C_BLC								"╰"
#define	C_URC								"╮"
#define	C_BRC								"╯"

#define D_H 								"═"
#define D_VHR 								"╞"
#define D_VHL 								"╡"
#define D_X									"╪"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define DISPLAY(...)						fprintf(stdout,  __VA_ARGS__)
#define FLUSH()								fflush(stdout)
#define SAVEINITSCREEN(n)					do { \
												tputs(screeninit, (int)n, putchar); \
											} while (0)
#define RESTOREINITSCREEN(n)				do { \
												tputs(screendeinit, (int)n, putchar); \
											} while (0)
#define CLEAR_ENTIRE_SCREEN()				tputs(clear, (int)nbLINES, putchar)
#define MOVE_CURSOR(line, column)			tputs(tgoto(move, ((int)(column - 1) % nbCOLS), (int)(line - 1)), 1, putchar)
// To migrate to portable mechanism (terminfo, tputs, ...) later!!!
#define MOVE_CURSOR_AND_CLEAR_LINE(x, y)	DISPLAY("\033[%d;%dH\033[K", x, y)
#define HIDE_CURSOR()						DISPLAY("\033[?25l")
#define SHOW_CURSOR()						DISPLAY("\033[?25h")
#define SET_CURSOR(x)						DISPLAY("\033[%d q", x)
#define DISPLAY_BELL()						do { \
												DISPLAY("\033[?5h"); FLUSH(); \
												usleep(50000); \
												DISPLAY("\033[?5l"); FLUSH(); \
											} while (0)
#define DISPLAY_AT(line, column, ...)		do { \
												MOVE_CURSOR(line, column); \
												fprintf(stdout,  __VA_ARGS__); \
											} while (0)

#define FOREGROUND(color) 					(30 + color)
#define BACKGROUND(color) 					(40 + color)
#define MAX(a,b)							(a > b ? a : b)
#define MIN(a,b)							(a > b ? b : a)

#define MAXWIDTH(n)							((size_t) nbCOLS  - (size_t) initial_col_number - (3 * n)) / n;
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define _cr 								-13
#define _previous_char						-12
#define _next_char							-11
#define _not_known							-10
#define _pf4								-9
#define _pf3								-8
#define _pf2								-7
#define _backspace							-6
#define _abort_session						-5
#define _go_next							-4
#define _go_previous						-3
#define _go_up								-2
#define _go_down							-1
#define _ok 								0
#define _goon 								1
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define MAX_VALUE_LENGTH					1024
#define MAX_LINE_LENGTH						1024
#define MAX_READ_LENGTH						2048

#define NOROWTOCOLORIZE						-1
#define NOCOLTOCOLORIZE						-1
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum { FALSE = 0, TRUE = 1 } boolean;
typedef enum { black = 0, red, green, yellow, blue, magenta, cyan, white } e_color;
typedef enum { MODE_ECHOED = 0, MODE_RAW } e_terminal;
typedef enum { JLEFT = 0, JCENTER, JRIGHT } e_justification;
typedef enum { EDIT = 0, VIEW } e_viewing_mode;
typedef enum { FANAIS = 0, FMARKDOWN, FOTHER } e_inputfile;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct s_tablecol {
	size_t width;
	e_justification justification;
};
struct s_tablecell {
	int endLine;						// Terminal line of end of cell value. Filled during display of the table
	int startCol;						// Terminal col of the start of the table column. Filled during display of the table
	char *ptdisplay;					// Temporary pointer. Used for display purpose
	char value[MAX_VALUE_LENGTH];		// Filled by user
};
struct s_tabledisplay {
	int startLineValue;					// Terminal line of start of value. Filled during display of the table
	int startColValue;					// Terminal col of the start of the value. Filled during display of the table
	int endColValue;					// Terminal col of the end of the table column. Filled during display of the table
	struct s_tabledisplay *previous;	// Previous line of the value. Filled during display of the table
	struct s_tabledisplay *next;		// Next line of the value. Filled during display of the table
};
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern int 						nbLINES;
extern int 						nbCOLS;
extern char 					*screeninit;		// Startup terminal initialization
extern char 					*screendeinit;		// Exit terminal de-initialization
extern char 					*clear;				// Clear screen
extern char 					*move;				// Cursor positioning
extern size_t					table_number_of_columns;
extern size_t					table_number_of_lines;
extern size_t					default_suggested_width;
extern e_justification			default_justification;
extern int 						initial_line_number;
extern int 						initial_col_number;
extern size_t 					current_table_line;
extern size_t 					current_tab_col;
extern size_t					maximal_column_width;
extern struct s_tabledisplay 	*ptCoordinatesCurrentValue;
extern struct s_tablecol 		*table_col;
extern struct s_tablecell 		**table_cell;
extern struct s_tabledisplay	**table_display;
extern size_t 					table_display_number_of_lines;
extern size_t					table_display_number_of_columns;
extern char						mdfile[];
extern char 					anaisfile[];
extern boolean					modification_done_and_not_saved;
extern e_viewing_mode			current_viewing_mode;
//------------------------------------------------------------------------------
// INTERNAL GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
// anais.c
extern void 					_anais_endScreen( void );
extern void 					_anais_allocateTables( size_t, size_t );
extern void 					_anais_allocateColumnTable( size_t );
extern void						_anais_allocateCellTable( size_t );
extern void 					_anais_allocateCell( size_t, size_t );
extern void 					_anais_initializeTable( size_t );
extern void 					_anais_freeTables( struct s_tablecol *, struct s_tablecell **, size_t );
extern void 					_anais_allocateDisplayTable( void );
extern struct s_tabledisplay 	*_anais_allocateDisplayLine( void );
extern void 					_anais_freeDisplayTable( void );
// anais_funcs.c
extern void						_anais_fatal( const char *, ... );
extern void						_anais_getTerminalCapabilities( void );
extern void 					_anais_getTerminalSize( int *, int * );
extern void						_anais_setTerminalMode( e_terminal );
extern void 					_anais_getCursorPosition( int *, int *);
extern size_t 					_anais_count_characters_out_escaped_codes( char * );
extern size_t 					_anais_compute_index_out_escaped_codes( char *, size_t );
extern size_t 					_anais_count_number_of_effective_characters( char * );
// anais_display.c
extern void 					_anais_displayTable( e_viewing_mode, int, int );
extern void 					_anais_clearScreen( void );
extern void						_anais_save_indicator( void );
// anais_input.c
extern void 					_anais_fillTable( void );
extern void						_anais_moveCursor( void );
// anais_menu.c
extern int 						_anais_menu( int, int, char **, size_t, char * );
extern int 						_anais_incomingKey( void );
// anais_error.c
extern void					 	_anais_error( char * );
extern void 					_anais_displayTop( int *, int, size_t, char * );
extern void 					_anais_displayBottom( int, int, size_t  );
extern void 					_anais_displayEmptyLine( int *, int, size_t  );
extern void 					_anais_displayLabel( int *, int, size_t, char * );
// anais_gonogo.c
extern boolean					_anais_gonogo( char *, char *, char * );
// anais_file.c
extern void 					_anais_writeMDfile(void);
extern void 					_anais_readMDfile( void );
#ifdef ANAIS_FILE
extern void 					_anais_writeANAISfile(void);
extern void 					_anais_readANAISfile( void );
#endif
//------------------------------------------------------------------------------
#endif	// ANAIS_DATA_H
//==============================================================================
