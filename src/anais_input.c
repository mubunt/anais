//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define BS 					0x08
#define TAB 				0x09
#define CR 					0x0d
#define ESCAPE				0x1b
#define ARROW_UP			0x41
#define ARROW_DOWN			0x42
#define ARROW_RIGHT			0x43
#define ARROW_LEFT			0x44
#define O 					0x4f
#define Q 					0x51
#define R 					0x52
#define S 					0x53
#define OPENBRACKET			0x5b
#define TILDA 				0x7e
#define DEL 				0x7f
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static int incomingChar( void ) {
	int c = getchar();
	switch (c) {
	case BS:
	case DEL:
		c = _backspace;
		break;
	case CR:
	case TAB:
		c = _cr;
		break;
	case ESCAPE:
		c = getchar();
		switch (c) {
		case OPENBRACKET:
			c = getchar();
			switch (c) {
			case ARROW_RIGHT:
				c = _go_next;
				break;
			case ARROW_LEFT:
				c = _go_previous;
				break;
			case ARROW_DOWN:
				c = _go_down;
				break;
			case ARROW_UP:
				c = _go_up;
				break;
			case '1':
				c = getchar();
				switch (c) {
				case ';':
					c = getchar();
					switch (c) {
					case '2':
						c = getchar();
						switch (c) {
						case ARROW_RIGHT:
							c = _next_char;
							break;
						case ARROW_LEFT:
							c = _previous_char;
							break;
						default:
							c = _not_known;
							break;
						}
						break;
					default:
						c = _not_known;
						break;
					}
					break;
				default:
					c = _not_known;
					break;
				}
				break;
			default:
				c = _not_known;
				break;
			}
			break;
		case O:
			c = getchar();
			switch (c) {
			case Q:
				c = _pf2;
				break;
			case R:
				c = _pf3;
				break;
			case S:
				c = _pf4;
				break;
			default:
				c = _not_known;
				break;
			}
			break;
		default:
			c = _not_known;
			break;
		}
		break;
	default:
		break;
	}
	return(c);
}
//------------------------------------------------------------------------------
static size_t add_column( size_t icolumn, size_t milestone ) {
	struct s_tablecol *ex_table_col = table_col;
	struct s_tablecell **ex_table_cell = table_cell;

	++table_number_of_columns;
	maximal_column_width =  MAXWIDTH(table_number_of_columns);
	if (maximal_column_width <= 3) {
		_anais_error((char *) "Cannot add a new column; the table will be wider than the terminal.");
		--table_number_of_columns;
		return(icolumn);
	}
	_anais_allocateColumnTable(table_number_of_columns);
	for (size_t j = 0; j < milestone; j++) {
		table_col[j].width =  MIN(ex_table_col[j].width, maximal_column_width);
		table_col[j].justification = ex_table_col[j].justification;
	}
	table_col[milestone].width = default_suggested_width;
	table_col[milestone].justification = default_justification;
	for (size_t j = milestone + 1; j < table_number_of_columns; j++) {
		table_col[j].width =  MIN(ex_table_col[j - 1].width, maximal_column_width);
		table_col[j].justification = ex_table_col[j - 1].justification;
	}
	_anais_allocateCellTable(table_number_of_lines);
	for (size_t i = 0; i < table_number_of_lines; i++) {
		_anais_allocateCell(i, table_number_of_columns);
		for (size_t j = 0; j < milestone; j++) {
			strcpy(table_cell[i][j].value, ex_table_cell[i][j].value);
		}
		table_cell[i][milestone].value[0] = '\0';
		for (size_t j = milestone + 1; j < table_number_of_columns; j++) {
			strcpy(table_cell[i][j].value, ex_table_cell[i][j - 1].value);
		}
	}
	_anais_freeTables(ex_table_col, ex_table_cell, table_number_of_lines);
	modification_done_and_not_saved = TRUE;
	_anais_save_indicator();
	return(icolumn + 1);
}
//------------------------------------------------------------------------------
static size_t remove_column( size_t icolumn ) {
	struct s_tablecol *ex_table_col = table_col;
	struct s_tablecell **ex_table_cell = table_cell;

	if (table_number_of_columns == 1) {
		_anais_error((char *) "Can not remove a column from a one-column table.");
		return(icolumn);
	}
	--table_number_of_columns;
	maximal_column_width =  MAXWIDTH(table_number_of_columns);
	_anais_allocateColumnTable(table_number_of_columns);
	for (size_t j = 0; j < icolumn; j++) {
		table_col[j].width = ex_table_col[j].width;
		table_col[j].justification = ex_table_col[j].justification;
	}
	for (size_t j = icolumn + 1; j < table_number_of_columns + 1; j++) {
		table_col[j - 1].width = ex_table_col[j].width;
		table_col[j - 1].justification = ex_table_col[j].justification;
	}
	_anais_allocateCellTable(table_number_of_lines);
	for (size_t i = 0; i < table_number_of_lines; i++) {
		_anais_allocateCell(i, table_number_of_columns);
		for (size_t j = 0; j < icolumn; j++) {
			strcpy(table_cell[i][j].value, ex_table_cell[i][j].value);
		}
		for (size_t j = icolumn + 1; j < table_number_of_columns + 1; j++) {
			strcpy(table_cell[i][j - 1].value, ex_table_cell[i][j].value);
		}
	}
	_anais_freeTables(ex_table_col, ex_table_cell, table_number_of_lines);
	if (icolumn == table_number_of_columns) --icolumn;
	modification_done_and_not_saved = TRUE;
	_anais_save_indicator();
	return(icolumn);
}
//------------------------------------------------------------------------------
static size_t add_row( size_t milestone) {
	struct s_tablecell **ex_table_cell = table_cell;
	++table_number_of_lines;
	_anais_allocateCellTable(table_number_of_lines);
	for (size_t i = 0; i < milestone; i++) {
		_anais_allocateCell(i, table_number_of_columns);
		for (size_t j = 0; j < table_number_of_columns; j++) {
			strcpy(table_cell[i][j].value, ex_table_cell[i][j].value);
		}
	}
	_anais_allocateCell(milestone, table_number_of_columns);
	for (size_t j = 0; j < table_number_of_columns; j++) {
		table_cell[milestone][j].value[0] = '\0';
	}
	for (size_t i = milestone; i < table_number_of_lines - 1; i++) {
		_anais_allocateCell(i + 1, table_number_of_columns);
		for (size_t j = 0; j < table_number_of_columns; j++) {
			strcpy(table_cell[i + 1][j].value, ex_table_cell[i][j].value);
		}
	}
	_anais_freeTables(NULL, ex_table_cell, table_number_of_lines - 1);
	modification_done_and_not_saved = TRUE;
	_anais_save_indicator();
	return(milestone);
}
//------------------------------------------------------------------------------
static size_t remove_row( size_t irow ) {
	struct s_tablecell 		**ex_table_cell = table_cell;
	if (table_number_of_lines == 1) {
		_anais_error((char *) "Can not remove a row from a one-rowtable.");
		return(irow);
	}
	--table_number_of_lines;
	if (irow == table_number_of_lines) --irow;
	modification_done_and_not_saved = TRUE;
	_anais_save_indicator();
	return(irow);
}
//------------------------------------------------------------------------------
static int ssmenu_general() {
	const char *general_menu_title	= "MAIN MENU";
#ifdef ANAIS_FILE
	const char *general_menu[5]		= { "", "Save Markdown file", "Save Anais file", "Exit Session", "Save and Exit" };
#else
	const char *general_menu[4]		= { "", "Save file", "Exit Session", "Save and Exit" };
#endif

	if (current_viewing_mode == VIEW)
		general_menu[0] = "Edit Mode";
	else
		general_menu[0] = "View Mode";

	size_t nbl = sizeof(general_menu) / sizeof(general_menu[1]);
	int l = (nbLINES - ((int) nbl + 4)) / 2;
	size_t maxwidth = 0;
	for (size_t i = 0; i < nbl; i++) maxwidth = MAX(maxwidth, strlen(general_menu[i]));
	int c = (nbCOLS - ((int) maxwidth + 2)) / 2;
#ifdef ANAIS_FILE
	int ifct = _anais_menu(l, c, (char **) general_menu, 5, (char *) general_menu_title);
#else
	int ifct = _anais_menu(l, c, (char **) general_menu, 4, (char *) general_menu_title);
#endif
	int n;
	switch (ifct) {
	case 0:
		current_viewing_mode = (current_viewing_mode == VIEW) ? EDIT : VIEW;
		_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
		n = _goon;
		break;
	case 1:
		_anais_writeMDfile();
		modification_done_and_not_saved = FALSE;
		_anais_save_indicator ();
		_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
		n = _goon;
		break;
	case 2:
#ifdef ANAIS_FILE
		_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
		_anais_writeANAISfile();
		modification_done_and_not_saved = FALSE;
		_anais_save_indicator ();
		n = _goon;
		break;
	case 3:
#endif
		_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
		if (modification_done_and_not_saved) {
			if (! _anais_gonogo( (char *) "No save since last change.", (char *) "EXIT", (char *) "SAVE" )) {
#ifdef ANAIS_FILE
				_anais_writeANAISfile();
#endif
				_anais_writeMDfile();
			}
		}
		n = ANAIS_OK;
		break;

#ifdef ANAIS_FILE
	case 4:
		_anais_writeANAISfile();
#else
	case 3:
#endif
		_anais_writeMDfile();
		n = ANAIS_OK;
		break;
	default:
		_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
		n = _goon;
		break;
	}
	return n;
}
//------------------------------------------------------------------------------
static int ssmenu_rowcolumn() {
	const char *column_menu_title		= "COLUMNS";
	const char *column_menu[]			= { "Add Column to the Left", "Add Column to the Right", "Remove Column"};
	const char *row_column_menu_title	= "ROWS AND COLUMNS";
	const char *row_column_menu[]		= { "Add Row Above", "Add Row Below", "Remove Row", "Add Column to the Left", "Add Column to the Right", "Remove Column"};

	_anais_displayTable(current_viewing_mode, (int) current_table_line, (int) current_tab_col);
	if (current_table_line == 0) {
		size_t nbl = sizeof(column_menu) / sizeof(column_menu[0]);
		int l = (nbLINES - ((int) nbl + 4)) / 2;
		size_t maxwidth = 0;
		for (size_t i = 0; i < nbl; i++) maxwidth = MAX(maxwidth, strlen(column_menu[i]));
		int c = (nbCOLS - ((int) maxwidth + 2)) / 2;
		int ifct = _anais_menu(l, c, (char **) column_menu, 3, (char *) column_menu_title);
		switch (ifct) {
		case 0:
			current_tab_col = add_column( current_tab_col, current_tab_col);
			break;
		case 1:
			current_tab_col = add_column(current_tab_col, current_tab_col + 1);
			break;
		case 2:
			current_tab_col = remove_column(current_tab_col);
			break;
		default:
			break;
		}
	} else {
		size_t nbl = sizeof(row_column_menu) / sizeof(row_column_menu[0]);
		int l = (nbLINES - ((int) nbl + 4)) / 2;
		size_t maxwidth = 0;
		for (size_t i = 0; i < nbl; i++) maxwidth = MAX(maxwidth, strlen(row_column_menu[i]));
		int c = (nbCOLS - ((int) maxwidth + 2)) / 2;
		int ifct = _anais_menu(l, c, (char **) row_column_menu, 6, (char *) row_column_menu_title);
		switch (ifct) {
		case 0:
			current_table_line = add_row(current_table_line);
			current_tab_col = 0;
			break;
		case 1:
			current_table_line = add_row(current_table_line + 1);
			current_tab_col = 0;
			break;
		case 2:
			current_table_line = remove_row(current_table_line);
			break;
		case 3:
			current_tab_col = add_column(current_tab_col, current_tab_col);
			break;
		case 4:
			current_tab_col = add_column(current_tab_col, current_tab_col + 1);
			break;
		case 5:
			current_tab_col = remove_column(current_tab_col);
			break;
		default:
			break;
		}
	}
	_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
	return _goon;
}
//------------------------------------------------------------------------------
static int ssmenu_align() {
	const char *align_menu_title	= "JUSTIFICATION";
	const char *align_menu[]		= { "Align Column to the Left", "Center Column", "Align Column to the Right"};

	_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, (int) current_tab_col);
	size_t nbl = sizeof(align_menu) / sizeof(align_menu[0]);
	int l = (nbLINES - ((int) nbl + 4)) / 2;
	size_t maxwidth = 0;
	for (size_t i = 0; i < nbl; i++) maxwidth = MAX(maxwidth, strlen(align_menu[i]));
	int c = (nbCOLS - ((int) maxwidth + 2)) / 2;
	int ifct = _anais_menu(l, c, (char **) align_menu, 3, (char *) align_menu_title);
	switch (ifct) {
	case 0:
		modification_done_and_not_saved = table_col[current_tab_col].justification != JLEFT;
		table_col[current_tab_col].justification = JLEFT;
		break;
	case 1:
		modification_done_and_not_saved = table_col[current_tab_col].justification != JCENTER;
		table_col[current_tab_col].justification = JCENTER;
		break;
	case 2:
		modification_done_and_not_saved = table_col[current_tab_col].justification != JRIGHT;
		table_col[current_tab_col].justification = JRIGHT;
		break;
	default:
		break;
	}
	_anais_save_indicator();
	_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
	return _goon;
}
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define NEXT()				ptCurrentValue = table_cell[current_table_line][current_tab_col].value; \
							indexCurrentValue = strlen(table_cell[current_table_line][current_tab_col].value); \
							columnCurrentValue = 0; \
							ptCoordinatesCurrentValue = table_display[current_table_line * table_number_of_columns + current_tab_col]; \
							if (ptCoordinatesCurrentValue != NULL) { \
								while (ptCoordinatesCurrentValue->next != NULL) ptCoordinatesCurrentValue = ptCoordinatesCurrentValue->next; \
								columnCurrentValue = ptCoordinatesCurrentValue->endColValue; \
							} \
							_anais_moveCursor();

#define SETCURSOR(l, c)		MOVE_CURSOR(l, c); FLUSH();

#define INDEX2COL()			ptCoordinatesCurrentValue = table_display[current_table_line * table_number_of_columns + current_tab_col]; \
							columnCurrentValue = 0; \
							if (ptCoordinatesCurrentValue != NULL) { \
								size_t len = 0; \
								while (indexCurrentValue >= len + (size_t) (ptCoordinatesCurrentValue->endColValue - ptCoordinatesCurrentValue->startColValue + 1)) { \
									len += (size_t) (ptCoordinatesCurrentValue->endColValue - ptCoordinatesCurrentValue->startColValue + 1); \
									if (ptCoordinatesCurrentValue->next == NULL) break; \
									ptCoordinatesCurrentValue = ptCoordinatesCurrentValue->next; \
								} \
								columnCurrentValue = (int) (indexCurrentValue - len) + ptCoordinatesCurrentValue->startColValue; \
								SETCURSOR(ptCoordinatesCurrentValue->startLineValue, columnCurrentValue); \
							} else { \
								_anais_moveCursor(); \
							}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _anais_fillTable( void ) {
	int n, ifct;
	size_t indexCurrentValue, temp;
	char *ptCurrentValue;
	int columnCurrentValue;

	NEXT();
	while (1) {
		n = incomingChar();
		switch (n) {
		case _not_known:
			DISPLAY_BELL();
			break;
		case _go_previous:
			if (current_tab_col == 0)
				DISPLAY_BELL();
			else {
				--current_tab_col;
				NEXT();
				SET_CURSOR(BLINKING_BLOCK);
			}
			n = _goon;
			break;
		case _go_next:
			if (current_tab_col == table_number_of_columns - 1)
				DISPLAY_BELL();
			else {
				++current_tab_col;
				NEXT();
				SET_CURSOR(BLINKING_BLOCK);
			}
			n = _goon;
			break;
		case _go_up:
			if (current_table_line == 0)
				DISPLAY_BELL();
			else {
				--current_table_line;
				NEXT();
				SET_CURSOR(BLINKING_BLOCK);
			}
			n = _goon;
			break;
		case _go_down:
			if (current_table_line == table_number_of_lines - 1)
				DISPLAY_BELL();
			else {
				++current_table_line;
				NEXT();
				SET_CURSOR(BLINKING_BLOCK);
			}
			n = _goon;
			break;
		case _previous_char:
			if (indexCurrentValue == 0) {
				DISPLAY_BELL();
			} else {
				--indexCurrentValue;
				--columnCurrentValue;
				if (columnCurrentValue < ptCoordinatesCurrentValue->startColValue) {
					ptCoordinatesCurrentValue = ptCoordinatesCurrentValue->previous;
					columnCurrentValue = ptCoordinatesCurrentValue->endColValue;
				}
				SETCURSOR(ptCoordinatesCurrentValue->startLineValue, columnCurrentValue);
				SET_CURSOR(BLINKING_BAR);
			}
			n = _goon;
			break;
		case _next_char:
			if (indexCurrentValue == strlen(table_cell[current_table_line][current_tab_col].value)) {
				DISPLAY_BELL();
			} else {
				++indexCurrentValue;
				++columnCurrentValue;
				if (columnCurrentValue > ptCoordinatesCurrentValue->endColValue) {
					ptCoordinatesCurrentValue = ptCoordinatesCurrentValue->next;
					columnCurrentValue = ptCoordinatesCurrentValue->startColValue;
				}
				SETCURSOR(ptCoordinatesCurrentValue->startLineValue, columnCurrentValue);
				SET_CURSOR(BLINKING_BAR);
			}
			n = _goon;
			break;
		case _cr:
			if (current_tab_col == table_number_of_columns - 1) {
				if (current_table_line == table_number_of_lines - 1)
					DISPLAY_BELL();
				else {
					++current_table_line;
					current_tab_col = 0;
					NEXT();
					SET_CURSOR(BLINKING_BLOCK);
				}
			} else {
				++current_tab_col;
				//_anais_moveCursor();
				NEXT();
				SET_CURSOR(BLINKING_BLOCK);
			}
			n = _goon;
			break;
		case _pf2:
			n = ssmenu_general();
			NEXT();
			SET_CURSOR(BLINKING_BLOCK);
			break;
		case _pf3:
			n = ssmenu_rowcolumn();
			NEXT();
			SET_CURSOR(BLINKING_BLOCK);
			break;
		case _pf4:
			n = ssmenu_align();
			SET_CURSOR(BLINKING_BLOCK);
			NEXT();
			break;
		case _backspace:
			temp = strlen(table_cell[current_table_line][current_tab_col].value);
			if (temp == 0) {
				DISPLAY_BELL();
			} else {
				for (size_t i = indexCurrentValue; i <= temp; i++)
					ptCurrentValue[i - 1] = ptCurrentValue[i];
				--indexCurrentValue;
				_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
				modification_done_and_not_saved = TRUE;
				_anais_save_indicator();
				INDEX2COL();
				SET_CURSOR(BLINKING_BAR);
			}
			n = _goon;
			break;
		default:
			temp = strlen(table_cell[current_table_line][current_tab_col].value);
			if (temp == MAX_VALUE_LENGTH - 1) {
				DISPLAY_BELL();
			} else {
				if (temp == 0) {
					ptCurrentValue[0] = (char) n;
					ptCurrentValue[1] = '\0';
					indexCurrentValue = 1;
				} else {
					for (int i = (int) temp; i >= (int) indexCurrentValue; i--)
						ptCurrentValue[i + 1] = ptCurrentValue[i];
					ptCurrentValue[indexCurrentValue] = (char) n;
					++indexCurrentValue;
				}
				table_col[current_tab_col].width = MAX(table_col[current_tab_col].width,
				                                       _anais_count_number_of_effective_characters(table_cell[current_table_line][current_tab_col].value));
				table_col[current_tab_col].width = MIN(table_col[current_tab_col].width, maximal_column_width);
				_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
				modification_done_and_not_saved = TRUE;
				_anais_save_indicator();
				INDEX2COL();
			}
			n = _goon;
			break;
		}
		if (n == ANAIS_OK) break;
	}
}
//------------------------------------------------------------------------------
void _anais_moveCursor() {
	size_t width = table_col[current_tab_col].width;
	if (ptCoordinatesCurrentValue == NULL) {
		if (current_table_line == 0) {
			MOVE_CURSOR(table_cell[current_table_line][current_tab_col].endLine, table_cell[current_table_line][current_tab_col].startCol + (int) width / 2);
		} else {
			switch (table_col[current_tab_col].justification) {
			case JCENTER:
				MOVE_CURSOR(table_cell[current_table_line][current_tab_col].endLine, table_cell[current_table_line][current_tab_col].startCol + (int) width / 2);
				break;
			case JRIGHT:
				MOVE_CURSOR(table_cell[current_table_line][current_tab_col].endLine, table_cell[current_table_line][current_tab_col].startCol + (int) width);
				break;
			case JLEFT:
				MOVE_CURSOR(table_cell[current_table_line][current_tab_col].endLine, table_cell[current_table_line][current_tab_col].startCol);
				break;
			}
		}
	} else {
		MOVE_CURSOR(ptCoordinatesCurrentValue->startLineValue, ptCoordinatesCurrentValue->endColValue);
	}
	FLUSH();
}
//==============================================================================
