//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// LOCAM VARIABLES
//------------------------------------------------------------------------------
static char gohighlighted[MAX_LINE_LENGTH];
static char nogohighlighted[MAX_LINE_LENGTH];
static int linehighlighted;
static int columnhighlighted;
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define INITFIELD(field, a)		do { \
									size_t p = (bwidth - strlen(a) - 2) / 2;  \
									strcpy(field, " "); \
									for (size_t i = 0; i < p; i++) strcat(field, " "); \
									strcat(field, a); \
									for (size_t i = 0; i < bwidth - strlen(a) - p - 2; i++) strcat(field, " "); \
									strcat(field, " "); \
								} while (0);
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void displaychoice(int *line, int column, size_t width, size_t bwidth, char *go, char *nogo ) {
	linehighlighted = *line;
	columnhighlighted = column;
	char *field1 = malloc((sizeof(char) * (bwidth + 1)));
	char *field2 = malloc((sizeof(char) * (bwidth + 1)));
	INITFIELD(field1, go);
	INITFIELD(field2, nogo);
	int p = (int) ((width - 2) - (2 * bwidth));

	sprintf(nogohighlighted, ESC_STRONG_ON "%s", C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(nogohighlighted, " ");
	strcat(nogohighlighted, ESC_REVERSE_ON);
	strcat(nogohighlighted, field2);
	strcat(nogohighlighted, ESC_ATTRIBUTSOFF);
	strcat(nogohighlighted, ESC_STRONG_ON);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(nogohighlighted, " ");
	strcat(nogohighlighted, field1);
	for (int i = 0; i < p / 3 ; i++) strcat(nogohighlighted, " ");
	strcat(nogohighlighted, C_V);
	strcat(nogohighlighted, ESC_ATTRIBUTSOFF);

	sprintf(gohighlighted, ESC_STRONG_ON "%s", C_V);
	for (int i = 0; i < p / 3 ; i++) strcat(gohighlighted, " ");
	strcat(gohighlighted, field2);
	for (int i = 0; i < p - 2 * (p / 3); i++) strcat(gohighlighted, " ");
	strcat(gohighlighted, ESC_REVERSE_ON);
	strcat(gohighlighted, field1);
	strcat(gohighlighted, ESC_ATTRIBUTSOFF);
	strcat(gohighlighted, ESC_STRONG_ON);
	for (int i = 0; i < p / 3 ; i++) strcat(gohighlighted, " ");
	strcat(gohighlighted, C_V);
	strcat(gohighlighted, ESC_ATTRIBUTSOFF);

	free(field1);
	free(field2);
	DISPLAY_AT((*line)++, column, "%s", nogohighlighted);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
boolean _anais_gonogo( char *question, char *go, char *nogo ) {
	// The width of the menu is the largest between the widths of,
	// the question, and the button line, plus the edges (2 * 2 characters)
	size_t buttonwidth = 4 + MAX(strlen(go), strlen(nogo));
	size_t width =  4 + MAX(strlen(question), 2 * buttonwidth + 2);
	// Position of the question / answer / menu / help. 6 = frame + empty line + buttons
	int line = (nbLINES -8) / 2;
	int col = (nbCOLS -  (int) width) / 2;
	// Display frame including question and choice buttons
	_anais_displayTop(&line, col, width, (char *) "");
	_anais_displayEmptyLine(&line, col, width);
	_anais_displayLabel(&line, col, width - 4, question);
	_anais_displayEmptyLine(&line, col, width);
	displaychoice(&line, col, width, buttonwidth, go, nogo);
	_anais_displayEmptyLine(&line, col, width);
	_anais_displayBottom(line, col,  width);
	FLUSH();
	// Read selection
	boolean selection = FALSE;
	HIDE_CURSOR();
	while (1) {
		int n = _anais_incomingKey();
		switch (n) {
		case _go_next:
			if (selection)
				DISPLAY_BELL();
			else {
				selection = TRUE;
				DISPLAY_AT(linehighlighted, columnhighlighted, "%s", gohighlighted);
			}
			break;
		case _go_previous:
			if (selection) {
				selection = FALSE;
				DISPLAY_AT(linehighlighted, columnhighlighted, "%s", nogohighlighted);
			} else
				DISPLAY_BELL();
			break;
		case _ok:
			n = ANAIS_OK;
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (n == ANAIS_OK) break;
	}
	SHOW_CURSOR();
	return selection;
}
//==============================================================================
