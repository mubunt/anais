//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define ANAIS_PACKAGE			"ANAIS"
#define ANAIS_PURPOSE			"A markdown table textual generator."
#ifdef VERSION
#define ANAIS_VERSION(x) 		str(x)
#define str(x)					#x
#else
#define ANAIS_VERSION 			"Unknown"
#endif
//------------------------------------------------------------------------------
// EXPORTABLE VARIABLES
//------------------------------------------------------------------------------
int 					nbLINES;
int 					nbCOLS;

char 					*screeninit;		// Startup terminal initialization
char 					*screendeinit;		// Exit terminal de-initialization
char 					*clear;				// Clear screen
char 					*move;				// Cursor positioning

size_t					table_number_of_lines			= 5;
size_t					table_number_of_columns			= 3;
size_t					default_suggested_width			= 4;
e_justification			default_justification			= JLEFT;

int 					initial_line_number				= 4;
int 					initial_col_number				= 2;

size_t 					current_table_line				= 0;
size_t 					current_tab_col					= 0;
size_t					maximal_column_width			= 0;
struct s_tabledisplay 	*ptCoordinatesCurrentValue;

struct s_tablecol 		*table_col;
struct s_tablecell		**table_cell;
struct s_tabledisplay	**table_display;
size_t 					table_display_number_of_lines;
size_t					table_display_number_of_columns;

char					mdfile[PATH_MAX]				= "";
char 					anaisfile[PATH_MAX]				= "";

boolean					modification_done_and_not_saved	= FALSE;
e_viewing_mode			current_viewing_mode			= VIEW;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static __sighandler_t 	initial_signal;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void sizemanagement( int sig ) {
	signal(SIGWINCH, SIG_IGN);
	// Get new terminal size
	_anais_getTerminalSize(&nbLINES, &nbCOLS);
	// Recompute the maximal width of each column
	maximal_column_width =  MAXWIDTH(table_number_of_columns);
	if (maximal_column_width <= 3) {
		_anais_error((char *) "Cannot display the table; it is wider than the terminal!");
	} else {
		for (size_t i = 0; i < table_number_of_columns; i++)
			table_col[i].width = MIN(table_col[i].width, maximal_column_width);
	}
	signal(SIGWINCH, sizemanagement);
	_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
	_anais_moveCursor();
	FLUSH();
}
//------------------------------------------------------------------------------
static void usage(char *exec) {
#ifdef ANAIS_FILE
	fprintf(stdout, "Usage: %s [-V | -h | <ANAIS | MARKDOWN file>]\n", exec);
	fprintf(stdout, "	with:\n\
		<ANAIS | MARKDOWN file>i: Specify the ANAIS ('.anais') or MARKDOWN ('.md') file to edit\n\
		-V: Display version and exit\n\
		-h: Display help and exit\n\n");
#else
	fprintf(stdout, "Usage: %s [-V | -h | <MARKDOWN file>]\n", exec);
	fprintf(stdout, "	with:\n\
		 <MARKDOWN file>: Specify the MARKDOWN ('.md') file to edit\n\
		-V: Display version and exit\n\
		-h: Display help and exit\n\n");
#endif
}
//------------------------------------------------------------------------------
static boolean existfile( char *file ) {
	struct stat locstat;
	if (file == NULL) return FALSE;
	if (stat(file, &locstat) < 0) return FALSE;
	return TRUE;
}

static boolean emptyfile( char *file ) {
	struct stat locstat;
	if (stat(file, &locstat) == 0) return locstat.st_size == 0;
	_anais_fatal( "Cannot stat file '%s'!!!", file);
	return FALSE;
}
//------------------------------------------------------------------------------
static void initScreen( void ) {
	// Ignore window size changes
	initial_signal = signal(SIGWINCH, SIG_IGN);
	// Get terminal capabilities (instead of using escape code)
	_anais_setTerminalMode(MODE_RAW);
	_anais_getTerminalCapabilities();
	// Get terminal size
	_anais_getTerminalSize(&nbLINES, &nbCOLS);
	// Save initial state of the terminal screen to be restore on end
	SAVEINITSCREEN(nbLINES);
	FLUSH();
	// Change size signal management
	signal(SIGWINCH, sizemanagement);
}
//------------------------------------------------------------------------------
static e_inputfile what_do_I_have_as_input_file( char *file) {
	char *dot = file + strlen(file) - 1;
	while (*dot != '.' && dot != file) dot--;
	if (*dot != '.' && dot == file) return FOTHER;
	*dot = '\0';
	strcpy(anaisfile, file);
	strcat(anaisfile, ANAIS_SUFFIX);
	strcpy(mdfile, file);
	strcat(mdfile, MD_SUFFIX);
	*dot = '.';
#ifdef ANAIS_FILE
	if (strcmp(dot, ANAIS_SUFFIX) == 0) return FANAIS;
#endif
	if (strcmp(dot, MD_SUFFIX) == 0) return FMARKDOWN;
	return FOTHER;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void _anais_endScreen( void ) {
	_anais_setTerminalMode(MODE_ECHOED);
	// Restore initial signal
	signal(SIGWINCH, initial_signal);
	// Restore initial screnn state
	RESTOREINITSCREEN(nbLINES);
	FLUSH();
}
//------------------------------------------------------------------------------
void _anais_allocateTables( size_t lines, size_t cols ) {
	_anais_allocateColumnTable(cols);
	_anais_allocateCellTable(lines);
	for (size_t i = 0; i < lines; i++) _anais_allocateCell(i, cols);
}

void _anais_allocateColumnTable( size_t cols ) {
	table_col = malloc(sizeof(struct s_tablecol) * cols);
}

void _anais_allocateCellTable( size_t lines ) {
	table_cell = malloc(sizeof(struct s_tablecell) * lines);
}

void _anais_allocateCell( size_t iline, size_t cols ) {
	table_cell[iline] = malloc(sizeof(struct s_tablecell) * cols);
}
//------------------------------------------------------------------------------
void _anais_allocateDisplayTable( void ) {
	table_display = malloc(sizeof(struct s_tabledisplay *) * table_number_of_lines * table_number_of_columns);
	for (size_t i = 0; i < table_number_of_lines * table_number_of_columns; i++) {
		table_display[i] = NULL;
	}
	table_display_number_of_lines = table_number_of_lines;
	table_display_number_of_columns = table_number_of_columns;
}

struct s_tabledisplay *_anais_allocateDisplayLine( void ) {
	struct s_tabledisplay *pt = malloc(sizeof(struct s_tabledisplay));
	pt->startLineValue = pt->startColValue = pt->endColValue = -1;
	pt->previous = pt->next = NULL;
	return pt;
}
//------------------------------------------------------------------------------
void _anais_initializeTable( size_t maxwidth ) {
	for (size_t i = 0; i < table_number_of_columns; i++) {
		table_col[i].width =  default_suggested_width;
		table_col[i].justification = default_justification;
	}
	for (size_t i = 0; i < table_number_of_lines; i++) {
		for (size_t j = 0; j < table_number_of_columns; j++) {
			table_cell[i][j].ptdisplay = NULL;
			table_cell[i][j].endLine = 0;
			table_cell[i][j].startCol = 0;
			table_cell[i][j].value[0] = '\0';
		}
	}
}
//------------------------------------------------------------------------------
void _anais_freeTables( struct s_tablecol *tablecol, struct s_tablecell **tablecell, size_t tablenumberoflines ) {
	if (tablecol != NULL)
		free(tablecol);
	if (tablecell != NULL) {
		for (size_t i = 0; i < tablenumberoflines; i++)
			free(tablecell[i]);
		free(tablecell);
	}
}
//------------------------------------------------------------------------------
void _anais_freeDisplayTable( void ) {
	if (table_display != NULL) {
		for (size_t i = 0; i < table_display_number_of_lines; i++) {
			for (size_t j = 0; j < table_display_number_of_columns; j++) {
				size_t n = i * table_display_number_of_columns + j;
				if (table_display[n] != NULL) {
					struct s_tabledisplay *pt = table_display[n];
					while (pt != NULL) {
						struct s_tabledisplay *ptbis = pt->next;
						free(pt);
						pt = ptbis;
					}
					table_display[n] = NULL;
				}
			}
		}
		free(table_display);
		table_display = NULL;
	}
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	extern char *optarg;
	extern int optind, opterr;
	int version_requested = 0, help_requested = 0, errflg = 0, c;
	//--------------------------------------------------------------------------
	char *progname = argv[0];
	char *file = NULL;
	//--------------------------------------------------------------------------
	// Parameter checking and setting
	opterr = 0;
	while ((c = getopt (argc, argv, "hV")) != -1) {
		switch (c) {
		case 'h':
			help_requested = 1;
			break;
		case 'V':
			version_requested = 1;
			break;
		case '?':
			errflg++;
			break;
		default:
			_anais_fatal("%s", "Abnormal situation during parameter checking. Abort!");
		}
	}
	if (errflg) {
		usage(progname);
		return(EXIT_FAILURE);
	}
	if (help_requested) {
		usage(progname);
		return(EXIT_SUCCESS);
	}
	if (version_requested) {
		fprintf(stdout, "%s - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.\n", ANAIS_PACKAGE);
		fprintf(stdout, "%s - %s - Version %s\n\n", ANAIS_PACKAGE, ANAIS_PURPOSE, ANAIS_VERSION(VERSION));
		return(EXIT_SUCCESS);
	}
	if (optind != argc) {
		file = argv[optind];
	}
	if (file == NULL) {
		usage(progname);
		return(EXIT_FAILURE);
	}
	//--------------------------------------------------------------------------
	e_inputfile inputfile = what_do_I_have_as_input_file(file);
	if (inputfile == FOTHER) {
		usage(progname);
		return(EXIT_SUCCESS);
	}
	//--------------------------------------------------------------------------
	// Signals
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
	// Screen initialization
	initScreen();
	// Table allocation
	table_col = NULL;
	table_cell = NULL;
	table_display = NULL;
	table_display_number_of_lines = table_display_number_of_columns = 0;
	// Read input file
	size_t maxwidth;
	if (existfile(file) && ! emptyfile(file)) {
#ifdef ANAIS_FILE
		switch (inputfile) {
		case FANAIS:
			_anais_readANAISfile();
			break;
		case FMARKDOWN:
#endif
			_anais_readMDfile();
#ifdef ANAIS_FILE
			break;
		default:
			break;
		}
#endif
		maximal_column_width =  MAXWIDTH(table_number_of_columns);
	} else {
		maximal_column_width =  MAXWIDTH(table_number_of_columns);
		_anais_allocateTables(table_number_of_lines, table_number_of_columns);
		_anais_initializeTable(maximal_column_width);
	}
	if (maximal_column_width <= 3) {
		_anais_freeTables(table_col, table_cell, table_number_of_lines);
		_anais_freeDisplayTable();
		_anais_fatal("%s", "Cannot initialize an empty table; it will be wider than the terminal!");
	}
	modification_done_and_not_saved = FALSE;
	_anais_save_indicator ();
	// Display
	current_viewing_mode = VIEW;
	_anais_displayTable(current_viewing_mode, NOROWTOCOLORIZE, NOCOLTOCOLORIZE);
	// Fill the table
	current_table_line = 0;
	current_tab_col = 0;
	_anais_fillTable();
	// Free allocated memory
	_anais_freeTables(table_col, table_cell, table_number_of_lines);
	_anais_freeDisplayTable();
	// Screen reinitialization
	_anais_endScreen();
	// Signals
	signal(SIGABRT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGINT, SIG_DFL);
}
//------------------------------------------------------------------------------
