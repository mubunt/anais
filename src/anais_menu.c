//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project ANAIS, a mark-down table textual generator.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM AND APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "anais_data.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define BS 					0x08
#define CR 					0x0d
#define ESCAPE				0x1b
#define ARROW_UP			0x41
#define ARROW_DOWN			0x42
#define ARROW_RIGHT			0x43
#define ARROW_LEFT			0x44
#define OPENBRACKET			0x5b
#define TILDA 				0x7e
#define DEL 				0x7f
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int 		idxline;
static int 		idxcol;
static int 		firstMenuLine;
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------
#define HIGHLIGHT(n)	sprintf(buffer, ESC_STRONG_ON "%s " ESC_REVERSE_ON "%-*s" ESC_ATTRIBUTSOFF ESC_STRONG_ON " %s" ESC_ATTRIBUTSOFF, \
    						C_V, (int)maxwidth, functions[n], C_V); \
						DISPLAY_AT(firstMenuLine + n, idxcol, "%s", buffer);
#define UNHIGHLIGHT(n)	sprintf(buffer, ESC_STRONG_ON "%s %-*s %s" ESC_ATTRIBUTSOFF, \
							C_V, (int)maxwidth, functions[n], C_V); \
						DISPLAY_AT(firstMenuLine + n, idxcol, "%s", buffer);
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static void display_menu( int iline, int icol, char *fct[], size_t nfct, size_t width, char *title) {
	char buffer[MAX_LINE_LENGTH];
	// Top
	sprintf(buffer, ESC_STRONG_ON "%s", C_ULC);
	for (size_t i = 0; i < width + 2; i++) strcat(buffer, C_H);
	strcat(buffer, C_URC);
	strcat(buffer, ESC_ATTRIBUTSOFF);
	DISPLAY_AT(iline++, icol, "%s", buffer);
	if (strlen(title) != 0) {
		size_t n = (width - strlen(title)) / 2;
		sprintf(buffer, ESC_STRONG_ON "%s%*s%s%*s%s" ESC_ATTRIBUTSOFF, C_V, (int) n, " ", title, (int) (width - strlen(title) - n + 2), " ", C_V);
		DISPLAY_AT(iline++, icol, "%s", buffer);
		sprintf(buffer, ESC_STRONG_ON "%s", D_VHR);
		for (size_t i = 0; i < width + 2; i++) strcat(buffer, D_H);
		strcat(buffer, D_VHL);
		strcat(buffer, ESC_ATTRIBUTSOFF);
		DISPLAY_AT(iline++, icol, "%s", buffer);
	}
	// Body
	firstMenuLine = iline;
	for (int i = 0; i < (int) nfct; i++) {
		sprintf(buffer, ESC_STRONG_ON "%s %-*s %s" ESC_ATTRIBUTSOFF, C_V, (int)width, fct[i], C_V);
		DISPLAY_AT(iline++, icol, "%s", buffer);
	}
	// Bottom
	sprintf(buffer, ESC_STRONG_ON "%s", C_BLC);
	for (size_t i = 0; i < width + 2; i++) strcat(buffer, C_H);
	strcat(buffer, C_BRC);
	strcat(buffer, ESC_ATTRIBUTSOFF);
	DISPLAY_AT(iline, icol, "%s", buffer);
	FLUSH();
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
int _anais_menu( int iline, int icol, char *functions[], size_t nfunctions, char *title ) {
	char buffer[MAX_LINE_LENGTH];
	idxline = iline;
	idxcol = icol;
	// Width of menu
	size_t maxwidth = 0;
	for (size_t i = 0; i < nfunctions; i++) maxwidth = MAX(maxwidth, strlen(functions[i]));
	maxwidth = MAX(maxwidth, strlen(title));
	// Test if menu wider or longer of the terminal.
	if (nbCOLS < (idxcol + (int) maxwidth + 2)) idxcol = nbCOLS - (int) maxwidth - 3;
	if (nbLINES < (idxline + (int) nfunctions + 4)) idxline = nbLINES - (int) nfunctions - 3;
	// Display menu
	display_menu(idxline, idxcol, functions, nfunctions, maxwidth, title);
	// Read selection
	int selection = 0;
	HIGHLIGHT(selection);
	HIDE_CURSOR();
	while (1) {
		int n = _anais_incomingKey();
		switch (n) {
		case _backspace:
		case _go_previous:
		case _go_next:
			selection = ANAIS_BACK;
			break;
		case _abort_session:
			selection = ANAIS_ABORT;
			break;
		case _go_up:
			UNHIGHLIGHT(selection);
			if (selection == 0)
				selection = (int) nfunctions - 1;
			else
				--selection;
			HIGHLIGHT(selection);
			break;
		case _go_down:
			UNHIGHLIGHT(selection);
			if (selection == (int) nfunctions - 1)
				selection = 0;
			else
				++selection;
			HIGHLIGHT(selection);
			break;
		case _ok:
			n = ANAIS_OK;
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (n == ANAIS_OK || selection == ANAIS_ABORT || selection == ANAIS_BACK) break;
	}
	SHOW_CURSOR();
	return selection;
}
//------------------------------------------------------------------------------
int _anais_incomingKey( void ) {
	int c = getchar();
	switch (c) {
	case BS:
	case DEL:
		c = _backspace;
		break;
	case CR:
		c = _ok;
		break;
	case ESCAPE:
		switch (getchar()) {
		case OPENBRACKET:
			switch (getchar()) {
			case ARROW_RIGHT:
				c = _go_next;
				break;
			case ARROW_LEFT:
				c = _go_previous;
				break;
			case ARROW_DOWN:
				c = _go_down;
				break;
			case ARROW_UP:
				c = _go_up;
				break;
			case '3':
				switch (getchar()) {
				case TILDA:
					c = _abort_session;
					break;
				default:
					c = _not_known;
					break;
				}
				break;
			default:
				c = _not_known;
				break;
			}
			break;
		default:
			c = _not_known;
			break;
		}
		break;
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		c = c & 0xf;
		break;
	default:
		c = _not_known;
	}
	return(c);
}
//==============================================================================
