# RELEASE NOTES:  *anais*, a mark-down table textual generator.
___
Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.2.12**:
  - Fixed typo in README file.

- **Version 1.2.11**:
  - Updated build system components.

- **Version 1.2.10**:
  - Updated build system.

- **Version 1.2.9**:
  - Removed unused files.

- **Version 1.2.8**:
  - Updated build system component(s)

- **Version 1.2.7**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.2.6**:
  - Some minor changes in .comment file(s).

- **Version 1.2.5**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.2.4**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.2.3**:
  - Added *git push* before *git push --tags*.

- **Version 1.2.2**:
  - Added tagging of new release.

- **Version 1.2.1**:
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.

- **Version 1.2.0**:
  - Moved from GPL v2 to GPL v3.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Improved version identification and optimization flags in Makefiles

- **Version 1.1.0**:
  - Fixed bug: save not recorded after a saving!.
  - Fixed bug: table badly displayed after adding a column.
  - Removed option "-i". Now usage is: anais [-V | -h | <MARKDOWN file>].
  - Added "Save and Exit" feature in F2 menu.

- **Version 1.0.0**:
  - First release.
