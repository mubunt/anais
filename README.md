# *anaïs*, a mark-down table textual generator.
***
**anaïs** is, as it was already written, a textual Mark-down table generator. By *'textual'*, one must understand non-graphical (in the Java sense, Java-script, etc.). However, the tables are presented in a form ...... very *'tabular'* :-). **anaïs** takes as input a mark-down description of a table (or an empty file) and proposes the generation of the table modified by the user.

In the rest of the document, we will write **anaïs** without umlaut. The line command does not contain an umlaut either.

*Objective of this development*: none specifically, except to have fun. However, some of the code could be reused in another context.

## LICENSE
**anais** is covered by the GNU General Public License (GPL) version 3 and above.

## EXAMPLES
### Source code
``` bash
| Decoder | Macro-Argument | Explanation |
| :------ | :------ | :------ |
| From file... |  |  |
|  | %d | Data file name. To ease the selection via the file browser, a predefined filter is set to the content of the **Suffix** variable defined in the property file. Example: --file=%d |
|  | %e | Second data file name. The predefined filter in the file browser is set to *.*. Example: --file=%d --exec=%e |
|  | %m | Additional options to be set by the user. Example: --file=%d --exec=%e %m |
| Direct connection... |  |  |
|  | %i | Internet Protocol (IP) address represented in dot-decimal notation, which consists of four decimal numbers, each ranging from 0 to 255, separated by dots, e.g., 172.16.254.1. Example: --ipaddress=%i |
|  | %p | Port number represented as a decimal number ranging from 1 to 65535. Example: --ipaddress=%i --port=%p |
|  | %m | Additional options to be set by the user. Example: --ipaddress=%i --port=%p %m |
```
### Result
![Example](./README_images/anais-01.png  "Example")

*Note:* The green block in the title area (first line of the screen) indicates that no changes have been made since the file was opened or since the last save. Otherwise, the block will be red.
## KEYBOARD COMMANDS

| Keyboard key | Action
| :----: | --------
| ⯅ | Go to the table/menu line above
| ⯆ | Go to the table/menu line below
| ⯇ | Go to the table column just to the left
| ⯈ | Go to the table column just to the right
| Shift ⯇ | Move the cursor back one character
| Shift ⯈ | Move the cursor one character
| Backspace | Delete the current character 
| Return | Goto next table cell
| Tab | Goto next table cell
| Key F2 | Main menu: Change mode (edit / display), save Mark-down file, exit session
| Key F3 | Rows and Columns menu: add row above, add row below, remove row, add column to the left, add column to the right, remove column
| Key F4 | Justification menu: align column to the left, center column, align column to the right

*Note 1:* If the cursor is on a column header, then the F3 menu will only offer the features related to the columns.

*Note 2:* There are 2 modes of presentation in **anais**. The *edit* mode allows you to edit and display the decoration characters ("\*\*", "\*", "\_\_", etc.). The *display* mode makes it possible to visualize the effect of these decoration characters; in this mode, editing, in the presence of characters, is not trivial! The F2 menu allows you to switch from one mode to another.

## USAGE
``` bash
$ anais --help
Usage: anais [-V | -h | <MARKDOWN file>]
	with:
		<MARKDOWN file> Specify the MARKDOWN ('.md') file to edit
		-V: Display version and exit
		-h: Display help and exit

$
```
## STRUCTURE OF THE APPLICATION
This section walks you through **anais**'s structure. Once you understand this structure, you will easily find your way around in **anais**'s code base.

``` bash
$ yaTree
├── README_images/      # Images for documentation
│   └── anais-01.png    # 
├── src/                # Source directory
│   ├── Makefile        # Makefile
│   ├── anais.c         # Main program
│   ├── anais_data.h    # Internal common header file
│   ├── anais_display.c # Table display
│   ├── anais_error.c   # Error display
│   ├── anais_file.c    # Read and Write file routines
│   ├── anais_funcs.c   # Common sub-routines
│   ├── anais_gonogo.c  # "Go-Nogo" display and management
│   ├── anais_input.c   # Input processing
│   └── anais_menu.c    # "Menu" display and management
├── COPYING.md          # GNU General Public License markdown file
├── LICENSE.md          # License markdown file
├── Makefile            # Makefile
├── README.md           # ReadMe Markdown file
├── RELEASENOTES.md     # Release Notes Mark-Down file
└── VERSION             # Version identification text file

2 directories, 17 files
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd anais
$ make clean all
```
## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd anais
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
$ anais -V
ANAIS - A markdown table textual generator. - Version 1.0.0

$ anais mytable.md
```
## NOTES
- In input markdown code,
   - There must be at least 3 dashes separating each header cell. The outer pipes (|) are optional.
   - Inline Markdown not supported.
   - Table without header cell not supported
- To display a Mark-down file in a textual but enriched form, you can use **willy**, a Linux Markdown text-formatting program producing output suitable for simple fixed-width terminal windows, located [here](https://github.com/mubunt/willy).

## SOFTWARE REQUIREMENTS
- For usage and development: *none*
- Developped and tested on XUBUNTU 18.04, GCC v7.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***