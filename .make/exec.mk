#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	C executable generation and processing for C and C/Java projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		PROGRAM 		Name of the executable to generate
#		VERSION 		Version of the executable to generate
#		SOURCES 		List of C source files
#		OBJDIR 			Relative pathname of the directory where object and executable files will be generated
#		OPTIM			Optimization options form compilation and link
#		LIBS 			Additional libraries to link (ld format)
#		STRIP 			Strip utility
#------------------------------------------------------------------------------
DEPDIR		= $(OBJDIR)/deps
OBJECTS		= $(addprefix $(OBJDIR)/,$(SOURCES:.c=.o))
EXEC		= $(OBJDIR)/$(PROGRAM)

CFLAGS		+= -DLINUX -D_REENTRANT -DVERSION=$(VERSION)
INCLUDES	+= -I. -I.. -I$(OBJDIR) -I$(INC_DIR)
#-------------------------------------------------------------------------------
CC			= gcc
MKDIR		= mkdir -p
RM			= rm -f
RMDIR		= rm -fr
INSTALL		= install -p -v -D
ASTYLE		= astyle
CPPCHECK 	= cppcheck
#-------------------------------------------------------------------------------
CCFLAGS		= $(CFLAGS) -m64 $(OPTIM) $(INCLUDES) -Wall -W -Wextra -Wno-unused -Wconversion -Wwrite-strings -Wstack-protector --std=c99 -D_GNU_SOURCE
DEPFLAGS	+= -MT $@ -MMD -MP -MF $(DEPDIR)/$(notdir $*).Td
LDFLAGS		+= -m64 $(OPTIM)
POSTCOMPILE	= mv -f $(DEPDIR)/$(notdir $*).Td $(DEPDIR)/$(notdir $*).d
#-------------------------------------------------------------------------------
all: $(DEPDIR) $(EXEC)
clean:
	@echo "-- Removing $(EXEC) $(OBJECTS)"
	$(MUTE)$(RM) $(EXEC) $(OBJECTS)
	$(MUTE)$(RM) $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES))) $(patsubst %,$(DEPDIR)/%.Td,$(basename $(SOURCES)))
	$(MUTE)if [ -d $(DEPDIR) ] && ! ls -A $(DEPDIR)/* > /dev/null 2>&1; then $(RMDIR) $(DEPDIR); fi
	$(MUTE)if [ -d $(OBJDIR) ] && ! ls -A $(OBJDIR)/* > /dev/null 2>&1; then $(RMDIR) $(OBJDIR); fi
install:
	@echo "-- Installing $(EXEC) to $(BIN_DIR)"
	$(MUTE)$(INSTALL) -m 755 $(EXEC) $(BIN_DIR)
cleaninstall:
	@echo "-- Removing $(PROGRAM) from $(BIN_DIR)"
	$(MUTE)$(RM) $(BIN_DIR)/$(PROGRAM)
astyle:
	@echo "-- Formatting all '.c' and '.h' files"
	$(MUTE)$(ASTYLE) --style=attach --indent=tab --align-pointer=name *.[ch]
cppcheck:
	@echo "-- Static C code analysis"
	$(MUTE)$(CPPCHECK) --std=c99 $(CFLAGS) $(INCLUDES) *.[ch]
#-------------------------------------------------------------------------------
$(EXEC): $(OBJECTS)
	@echo "-- Linking $@"
	$(MUTE)$(CC) $(LDFLAGS) -o $@ $(OBJECTS) $(LIBS)
	$(MUTE)$(STRIP) $@
$(OBJDIR)/%.o: %.c $(DEPDIR)/%.d
	@echo "-- Compiling $@"
	$(MUTE)$(CC) $(DEPFLAGS) $(CCFLAGS) -o $@ -c $(addsuffix .c,$(basename $(notdir $@)))
	$(MUTE)$(POSTCOMPILE)
$(DEPDIR)/%.d: ;
$(DEPDIR):
	$(MUTE)$(MKDIR) $(DEPDIR)
#-------------------------------------------------------------------------------
.PRECIOUS: $(DEPDIR)/%.d
.PHONY: all clean install cleaninstall astyle cppcheck
#-------------------------------------------------------------------------------
-include $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES)))
#-------------------------------------------------------------------------------
